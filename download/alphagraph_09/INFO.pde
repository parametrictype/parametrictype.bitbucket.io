/*
 :::::::::::::::::::::::::::::::
 ------------------------------
 PROJECT : alphaGraph : ARC  
 ------------------------------
 ::::::::::::::::::::::::::::::
 
 DEV 05/02/2018
 Updated for latest version of ControlP5
 Sketch : alphaGraph 
 
 - This software helps in generating ideas for letter forms ;–)
 
 NOTES TO SELF :
 
 - Write a clear set of instructions.
 
 *******************************************************
 * FIND A TITLE FOR THE SUBJECT... ALPHAGRAPH
 *******************************************************
 Written by MW2015
 www.freeartbureau.org
 
 INSTRUCTIONS
 ------------
 - Type text and play with various parameters.
 - MousePressed when "MOUSE" button has been activated deforms text. 
   The deform function is wired to mouse x & y position creating different effects accordingly.
 - ENTER > Clear screen & create a new grid.
 - exportPDF button exports to PDF.
 - SAVE & LOAD buttons enable one to save and load presets as separate files.
 
 ///////////////////////////// END
 
 
 * Copyright (c) 2015 Mark Webster
 * 
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * http://www.gnu.org/licenses/gpl.html
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 
 
 */